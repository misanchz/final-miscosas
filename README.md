# final-miscosas

## Datos

* Nombre: María Isabel Sánchez Sánchez
* Cuenta de laboratorio: misanchz
* Titulación: Ingeniería en Tecnologías de la Telecomunicación
* Despliegue: [http://misanchz.pythonanywhere.com/miscosas](http://misanchz.pythonanywhere.com/miscosas)
* Video básico: [https://youtu.be/UIUkvpbcmws](https://youtu.be/UIUkvpbcmws)
* Video parte opcional: [https://youtu.be/Cb6KVFNwicI](https://youtu.be/Cb6KVFNwicI)

## Cuenta Admin Site

* admin/1234

## Cuentas usuarios

* ana/anita1234
* juan/juanito1234
* isa/isita1234


## Resumen parte obligatoria

La práctica consiste en la creación de una aplicación web construida como un proyecto Django/Python3, que permitirá gestionar vídeos, noticias y otra información que los usuarios vayan encontrando por la red y les resulte interesante. La aplicación web tendrá las siguientes características:

* Para utilizar la página no hará falta autenticarse con una cuenta.
* Cuando un visitante quiera, se podrá crear una cuenta o iniciar sesión en una cuenta ya existente.
* Desde la página principal se podrá introducir el identificador (identificador del canal, nombre del artículo,...) para descargar los datos de ese alimentador.
* Todo usuario que haya iniciado sesión, podrá realizar tres acciones fundamentales sobre los items que aparezcan en la página: poner comentarios, Dar un voto positivo o un voto negativo y cambiar el estilo y el tamaño de la letra de la página.

## Lista de partes opcionales

* Inclusión de un favicon del sitio
* Inclusión de imágenes (no solo texto) en los comentarios 

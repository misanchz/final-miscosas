from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
import urllib.request
from .models import WkFeederItems, WkFeeders

items = 0

def addWkFeederItems(art_name, ch_title, ch_link, author, date):
    try:
        change = WkFeederItems.objects.get(article_name=art_name, change_title=ch_title, change_link=ch_link, change_author=author, change_date=date)
        change.article_name = art_name
        change.change_title = ch_title
        change.change_link = ch_link
        change.change_author = author
        change.change_date = date

    except WkFeederItems.DoesNotExist:
        change = WkFeederItems(article_name=art_name, change_title=ch_title, change_link=ch_link, change_author=author, change_date=date)
    change.save()

def addWkFeeder(art_name, art_link, items, update):
    try:
        article = WkFeeders.objects.get(article_name=art_name, article_link=art_link, items=items)
        article.article_name = art_name
        article.article_link = art_link
        article.items = items

        if not update:
            article.selected = True

    except WkFeeders.DoesNotExist:
        article = WkFeeders(article_name=art_name, article_link=art_link, items=items, selected=True)
    article.save()

def parserwkfeed(url, article_name, update):
    class WkHandlerFeed(ContentHandler):

        def __init__ (self):
            self.inChannel= False
            self.inItem = False
            self.inContent = False
            self.content = ""
            self.art_link = ""

        def startElement (self, name, attrs):
            if name == 'channel':
                self.inChannel = True

            elif name == 'item':
                self.inItem = True

            elif self.inChannel:
                if name == 'link':
                    self.inContent = True

        def endElement (self, name):
            global items

            if name == 'item':
                self.inItem = False
                items = items + 1

            elif name == 'channel':
                self.inChannel = False

                if (self.art_link != ""):
                    addWkFeeder(article_name, self.art_link, items, update)
                    items = 0

            elif (self.inChannel) and (not self.inItem):
                if name == 'link':
                    self.art_link = self.content

                self.content = ""
                self.inContent = False

        def characters (self, chars):
            if self.inContent:
                self.content = self.content + chars


    Parser = make_parser()
    Parser.setContentHandler(WkHandlerFeed())
    xmlStream = urllib.request.urlopen(url)
    Parser.parse(xmlStream)

def parserwkitems(url, article_name):
    class WkHandlerItems(ContentHandler):

        def __init__ (self):
            self.inItem = False
            self.inContent = False
            self.content = ""
            self.change_title = ""
            self.change_link = ""
            self.author = ""
            self.date = ""

        def startElement(self, name, attrs):

            if name == 'item':
                self.inItem = True

            elif self.inItem:
                if name == 'title':
                    self.inContent = True
                elif name == 'link':
                    self.inContent = True
                elif name == 'pubDate':
                    self.inContent = True
                elif name == 'dc:creator':
                    self.inContent = True

        def endElement (self, name):
            if name == 'item':
                self.inItem = False
                addWkFeederItems(article_name, self.change_title, self.change_link, self.author, self.date)

            elif self.inItem:
                if name == 'title':
                    self.change_title = self.content
                elif name == 'link':
                    self.change_link = self.content
                elif name == 'pubDate':
                    self.date = self.content
                elif name == 'dc:creator':
                    self.author = self.content

                self.content = ""
                self.inContent = False

        def characters (self, chars):
            if self.inContent:
                self.content = self.content + chars


    Parser = make_parser()
    Parser.setContentHandler(WkHandlerItems())
    xmlStream = urllib.request.urlopen(url)
    Parser.parse(xmlStream)

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
import urllib.request
from .models import YtFeederItems, YtFeeders

ch_name = ""
ch_link = ""
items = 0


def addYtFeederItems(ch_id, title, id, link, descr):
    try:
        video = YtFeederItems.objects.get(channel_id=ch_id, video_title=title, video_id=id, video_link=link, description=descr)
        video.channel_id = ch_id
        video.video_title = title
        video.video_id = id
        video.video_link = link
        video.description = descr

    except YtFeederItems.DoesNotExist:
        video = YtFeederItems(channel_id=ch_id, video_title=title, video_id=id, video_link=link, description=descr)
    video.save()

def addYtFeeder(ch_name, ch_id, ch_link, items, update):
    try:
        channel = YtFeeders.objects.get(channel_name=ch_name, channel_id=ch_id, channel_link=ch_link, items=items)
        channel.channel_name = ch_name
        channel.channel_id = ch_id
        channel.channel_link = ch_link
        channel.items = items

        if not update:
            channel.selected = True

    except YtFeeders.DoesNotExist:
        channel = YtFeeders(channel_name=ch_name, channel_id=ch_id, channel_link=ch_link, items=items, selected=True)
    channel.save()


def parseryt(url, ytfeeder_id, update):
    class YTHandler(ContentHandler):

        def __init__ (self):
            self.inFeed = False
            self.inEntry = False
            self.inContent = False
            self.inAuthor = False
            self.content = ""
            self.title = ""
            self.link = ""
            self.id = ""
            self.ch_name = ""
            self.ch_link = ""
            self.descr = ""

        def startElement (self, name, attrs):
            if name == 'feed':
                self.inFeed = True

            elif name == 'entry':
                self.inEntry = True

            elif name == 'author':
                self.inAuthor = True

            elif self.inEntry:
                if name == 'title':
                    self.inContent = True
                elif name == 'yt:videoId':
                    self.inContent = True
                elif name == 'media:description':
                    self.inContent = True
                elif name == 'link':
                    self.link = attrs.get('href')

            elif self.inAuthor:
                if name == 'name':
                    self.inContent = True
                elif name == 'uri':
                    self.inContent = True

        def endElement (self, name):
            global ch_name
            global ch_link
            global items

            if name == 'entry':
                self.inEntry = False
                addYtFeederItems(ytfeeder_id, self.title, self.id, self.link, self.descr)
                items = items + 1

            elif name == 'author':
                self.inAuthor = False
                if (self.ch_name != "") and (self.ch_link != ""):
                    ch_name = self.ch_name
                    ch_link = self.ch_link

            elif name == 'feed':
                self.inFeed = False
                addYtFeeder(ch_name, ytfeeder_id, ch_link, items, update)
                items = 0

            elif self.inAuthor:
                if name == 'name':
                    self.ch_name = self.content
                elif name == 'uri':
                    self.ch_link = self.content

                self.content = ""
                self.inContent = False

            elif self.inEntry:
                if name == 'title':
                    self.title = self.content
                elif name == 'yt:videoId':
                    self.id = self.content
                elif name == 'media:description':
                    self.descr = self.content

                self.content = ""
                self.inContent = False

        def characters (self, chars):
            if self.inContent:
                self.content = self.content + chars


    Parser = make_parser()
    Parser.setContentHandler(YTHandler())
    xmlStream = urllib.request.urlopen(url)
    Parser.parse(xmlStream)

from django.test import TestCase
from django.test import SimpleTestCase
from . import views
from .models import Profile, YtFeeders, WkFeeders, YtFeederItems, WkFeederItems

class TestViews(TestCase):

    def setUp(self):
        """Ejecutado antes de los test"""
        self.user = Profile(user='admin')
        self.ytfeeder = YtFeeders(channel_id='UC300utwSVAYOoRLEqmsprfg')
        self.wkfeeder = WkFeeders(article_name='Fuenlabrada')
        self.ytitem = YtFeederItems(channel_id='UC300utwSVAYOoRLEqmsprfg', video_id='HZOodD84MR8')
        self.wkitem = WkFeederItems(article_name='Fuenlabrada', change_title='Asqueladd: /* Education */', id=11)

#-----------------------------------PAGINA PRINCIPAL----------------------------------------------------------------------------------------------------------------------------------------------------------
    def test_get_ok_main(self):
        response = self.client.get('/miscosas/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.main)

        response = self.client.post('/miscosas/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.main)

    def test_get_ok_feeders(self):
        response = self.client.get('/miscosas/alimentadores')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.feeders)

        response = self.client.post('/miscosas/alimentadores')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.feeders)

    def test_get_ok_info(self):
        response = self.client.get('/miscosas/informacion')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.information)

    def test_get_ok_register(self):
        response = self.client.get('/miscosas/registro')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.register)

        response = self.client.post('/miscosas/registro')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.register)

    def test_get_ok_xml(self):
        response = self.client.get('/miscosas/%3Fformat=xml')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.main_xml)

    def test_get_ok_json(self):
        response = self.client.get('/miscosas/%3Fformat=json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.main_json)

#-----------------------------------USUARIOS-------------------------------------------------------------------------------------------------------------------------------------------------------------------

    def test_get_ok_users(self):
        response = self.client.get('/miscosas/usuarios')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.users)

    def test_get_ok_user(self):
        response = self.client.get('/miscosas/usuarios/' + self.user.user)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.user)

        response = self.client.post('/miscosas/usuarios/' + self.user.user)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.user)


#-----------------------------------ALIMENTADORES-------------------------------------------------------------------------------------------------------------------------------------------------------------------

    def test_get_ok_ytfeeder(self):
        response = self.client.get('/miscosas/youtube/' + self.ytfeeder.channel_id)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.yt_feeder)

        response = self.client.post('/miscosas/youtube/' + self.ytfeeder.channel_id)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.yt_feeder)

    def test_get_ok_wkfeeder(self):
        response = self.client.get('/miscosas/wikipedia/' + self.wkfeeder.article_name)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.wk_feeder)

        response = self.client.post('/miscosas/wikipedia/' + self.wkfeeder.article_name)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.wk_feeder)
        
#-----------------------------------ITEMS--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    def test_get_ok_ytitem(self):
        response = self.client.get('/miscosas/youtube/' + self.ytitem.channel_id + '/' + self.ytitem.video_id)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.yt_item)

        response = self.client.post('/miscosas/youtube/' + self.ytitem.channel_id + '/' + self.ytitem.video_id)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.yt_item)

    def test_get_ok_wkitem(self):
        response = self.client.get('/miscosas/wikipedia/' + self.wkitem.article_name + '/' + str(self.wkitem.id))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.wk_item)

        response = self.client.post('/miscosas/wikipedia/' + self.wkitem.article_name + '/' + str(self.wkitem.id))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.resolver_match.func, views.wk_item)

from django import forms
from .models import Profile, Comments

class AuthenticationForm(forms.Form):
    username = forms.CharField(required=False)
    password = forms.CharField(required=False, widget=forms.PasswordInput)

class YtFeederForm(forms.Form):
    Identificador_del_canal = forms.CharField(required=False)

class WkFeederForm(forms.Form):
    Nombre_del_articulo = forms.CharField(required=False)

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comments
        labels = {
            'body': "",
            'photo': 'Foto',
        }

        widgets ={
            'body': forms.Textarea(attrs={'rows':'10', 'cols': '130'}),
        }
        fields = ['body', 'photo']

class StyleForm(forms.Form):
    Estilo = forms.CharField(required=False)

class LetterForm(forms.Form):
    Tamaño_de_la_letra = forms.CharField(required=False)

class PhotoForm(forms.ModelForm):
    class Meta:
        model = Profile
        labels = {
            'photo': 'Foto',
        }

        fields = ['photo']

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.views.decorators.csrf import csrf_exempt
from .forms import AuthenticationForm, YtFeederForm, WkFeederForm, CommentForm, PhotoForm, StyleForm, LetterForm
from .models import YtFeederItems, YtFeeders, User, Comments, VotedItems, Profile, WkFeeders, WkFeederItems, TopPuntuationItems
from .ytparser import parseryt
from .wkparser import parserwkfeed, parserwkitems
from django.http import HttpResponseRedirect
from django.utils import timezone

in_main = True
update = False
ytitem = False
wkitem = False
message = ''
#-------------------------------------------- EN COMUN -------------------------------------------------------------------------------------------

@csrf_exempt
def main(request):
    global in_main
    global update
    global ytitem
    global wkitem

    form_auth = AuthenticationForm()
    form_yt = YtFeederForm()
    form_wk = WkFeederForm()

    if request.method == "POST":
        try:
            action = request.POST['action']
        except:
            return redirect('notfound')

        if action == "IniciarSesion":
            form_auth = AuthenticationForm(data=request.POST)
            if form_auth.is_valid():
                username = form_auth.cleaned_data['username']
                password = form_auth.cleaned_data['password']

                user = authenticate(username=username, password=password)

                if user is not None:
                    login(request, user)
                    return redirect('main')

        elif action == "EnviarYt":
            form_yt = YtFeederForm(request.POST)
            if form_yt.is_valid():
                ytfeeder_id = form_yt.cleaned_data['Identificador_del_canal']
                url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' + ytfeeder_id
                update = False
                parseryt(url, ytfeeder_id, update)

        elif action == "EliminarYt":
            ytid = request.POST['ytid']
            ytfeeder = YtFeeders.objects.get(channel_id=ytid)
            ytfeeder.selected = False
            ytfeeder.save(update_fields=['selected'])

        elif action == "SeleccionarYt":
            ytid = request.POST['ytid']
            ytfeeder = YtFeeders.objects.get(channel_id=ytid)
            ytfeeder.selected = True
            ytfeeder.save(update_fields=['selected'])

        elif action == "EnviarWk":
            form_wk = WkFeederForm(request.POST)
            if form_wk.is_valid():
                article_name = form_wk.cleaned_data['Nombre_del_articulo']
                url = 'https://en.wikipedia.org/w/index.php?title=' + article_name + '&action=history&feed=rss'
                update = False
                parserwkfeed(url, article_name, update)
                parserwkitems(url, article_name)

        elif action == "SeleccionarWk":
            art_name = request.POST['art_name']
            wkfeeder = WkFeeders.objects.get(article_name=art_name)
            wkfeeder.selected = True
            wkfeeder.save(update_fields=['selected'])

        elif action == "EliminarWk":
            art_name = request.POST['art_name']
            wkfeeder = WkFeeders.objects.get(article_name=art_name)
            wkfeeder.selected = False
            wkfeeder.save(update_fields=['selected'])

        if request.user.is_authenticated:
            if action == 'MeGusta':
                item_link = request.POST['item_link']
                voted_item = VotedItems.objects.get(user=request.user.username, item_link=item_link)

                try:
                    yt_item = YtFeederItems.objects.get(video_link=item_link)
                    ytitem = True
                    wkitem = False
                except YtFeederItems.DoesNotExist:
                    wk_item = WkFeederItems.objects.get(change_link=item_link)
                    ytitem = False
                    wkitem = True

                if voted_item.n_times_like == 0:
                    if voted_item.n_times_dislike == 0:
                        p = Profile.objects.get(user=request.user.username)
                        p.n_votes = p.n_votes + 1
                        p.save(update_fields=["n_votes"])
                    elif voted_item.n_times_dislike == 1:
                        if ytitem:
                            if yt_item.dislikes > 0:
                                yt_item.dislikes = yt_item.dislikes - 1
                        elif wkitem:
                            if wk_item.dislikes > 0:
                                wk_item.dislikes = wk_item.dislikes - 1

                    voted_item.n_times_like = 1
                    voted_item.n_times_dislike = 0
                    voted_item.date = timezone.now()
                    voted_item.save(update_fields=['n_times_dislike', 'n_times_like','date'])

                    if ytitem and (not wkitem):
                        yt_feeder = YtFeeders.objects.get(channel_id=yt_item.channel_id)

                        yt_item.likes = yt_item.likes + 1

                        if yt_item.likes > yt_item.dislikes:
                            yt_item.puntuation = yt_item.likes - yt_item.dislikes
                        else:
                            yt_item.puntuation = 0

                        yt_feeder.puntuation = yt_feeder.puntuation + 1

                        yt_item.save(update_fields=['likes','dislikes','puntuation'])
                        yt_feeder.save(update_fields=['puntuation'])

                    elif wkitem and (not ytitem):
                        wk_feeder = WkFeeders.objects.get(article_name=wk_item.article_name)

                        wk_item.likes = wk_item.likes + 1

                        if wk_item.likes > wk_item.dislikes:
                            wk_item.puntuation = wk_item.likes - wk_item.dislikes
                        else:
                            wk_item.puntuation = 0

                        wk_feeder.puntuation = wk_feeder.puntuation + 1

                        wk_item.save(update_fields=['likes','dislikes','puntuation'])
                        wk_feeder.save(update_fields=['puntuation'])

            elif action == 'NoMeGusta':
                item_link = request.POST['item_link']                
                voted_item = VotedItems.objects.get(user=request.user.username, item_link=item_link)

                try:
                    yt_item = YtFeederItems.objects.get(video_link=item_link)
                    ytitem = True
                    wkitem = False
                except YtFeederItems.DoesNotExist:
                    wk_item = WkFeederItems.objects.get(change_link=item_link)
                    ytitem = False
                    wkitem = True

                if voted_item.n_times_dislike == 0:
                    if voted_item.n_times_like == 0:
                        p = Profile.objects.get(user=request.user.username)
                        p.n_votes = p.n_votes + 1
                        p.save(update_fields=["n_votes"])
                    elif voted_item.n_times_like == 1:
                        if ytitem:
                            if yt_item.likes > 0:
                                yt_item.likes = yt_item.likes - 1
                        elif wkitem:
                            if wk_item.likes > 0:
                                wk_item.likes = wk_item.likes - 1

                    voted_item.n_times_dislike = 1
                    voted_item.n_times_like = 0
                    voted_item.date = timezone.now()
                    voted_item.save(update_fields=['n_times_like', 'n_times_dislike','date'])

                    if ytitem and (not wkitem):
                        yt_feeder = YtFeeders.objects.get(channel_id=yt_item.channel_id)

                        yt_item.dislikes = yt_item.dislikes + 1

                        if yt_item.likes > yt_item.dislikes:
                            yt_item.puntuation = yt_item.likes - yt_item.dislikes
                        else:
                            yt_item.puntuation = 0

                        if yt_feeder.puntuation > 0:
                            yt_feeder.puntuation = yt_feeder.puntuation - 1

                        yt_item.save(update_fields=['dislikes','likes','puntuation'])
                        yt_feeder.save(update_fields=['puntuation'])

                    elif wkitem and (not ytitem):
                        wk_feeder = WkFeeders.objects.get(article_name=wk_item.article_name)

                        wk_item.dislikes = wk_item.dislikes + 1

                        if wk_item.likes > wk_item.dislikes:
                            wk_item.puntuation = wk_item.likes - wk_item.dislikes
                        else:
                            wk_item.puntuation = 0

                        if wk_feeder.puntuation > 0:
                            wk_feeder.puntuation = wk_feeder.puntuation - 1

                        wk_item.save(update_fields=['likes','dislikes','puntuation'])
                        wk_feeder.save(update_fields=['puntuation'])
    else:
        ytfeeder_id = ""
        selectedyt = ""
        article_name = ""
        action = ""

    in_main = True
    ytfeeders = YtFeeders.objects.all()
    wkfeeders = WkFeeders.objects.all()
    selectedyt = ytfeeders.filter(selected=True)
    selectedwk = wkfeeders.filter(selected=True)

    ytitems_organized = YtFeederItems.objects.all().order_by('-puntuation')
    top10_ytitems = ytitems_organized.filter()[:10]
    wkitems_organized = WkFeederItems.objects.all().order_by('-puntuation')
    top10_wkitems = wkitems_organized.filter()[:10]

    for item in top10_ytitems:
        url_item_page = 'http://localhost:8000/miscosas/youtube/' + item.channel_id + '/' + item.video_id
        try:
            top = TopPuntuationItems.objects.get(item_name=item.video_title)
            top.item_name = item.video_title
            top.item_link = item.video_link
            top.likes = item.likes
            top.dislikes = item.dislikes
            top.puntuation = item.puntuation
            top.item_page = url_item_page
        except TopPuntuationItems.DoesNotExist:
            top = TopPuntuationItems(item_name=item.video_title, item_link=item.video_link, likes=item.likes, dislikes=item.dislikes, puntuation=item.puntuation, item_page=url_item_page)
        top.save()

    for item in wkitems_organized:
        url_item_page = 'http://localhost:8000/miscosas/wikipedia/' + item.article_name + '/' + str(item.id)
        try:
            top = TopPuntuationItems.objects.get(item_name=item.change_title)
            top.item_name = item.change_title
            top.item_link = item.change_link
            top.likes = item.likes
            top.dislikes = item.dislikes
            top.puntuation = item.puntuation
            top.item_page = url_item_page
        except TopPuntuationItems.DoesNotExist:
            top = TopPuntuationItems(item_name=item.change_title, item_link=item.change_link, likes=item.likes, dislikes=item.dislikes, puntuation=item.puntuation, item_page=url_item_page)
        top.save()

    top_organized = TopPuntuationItems.objects.filter(puntuation__gt=0).order_by('-puntuation')
    top10 = top_organized.filter()[:10]
    votes_organized = VotedItems.objects.filter(user=request.user.username, date__isnull=False).order_by('-date')
    last5_votes = votes_organized.filter()[:5]

    if request.user.is_authenticated:
        for item in top10:
            try:
                v = VotedItems.objects.get(user=request.user.username, item_link=item.item_link)
                v.user = request.user.username
                v.item_title = item.item_name
                v.item_link = item.item_link
                v.item_page = item.item_page
            except VotedItems.DoesNotExist:
                v = VotedItems(user=request.user.username, item_title=item.item_name, item_link=item.item_link, item_page=item.item_page)
            v.save()

    voted_items = VotedItems.objects.all().filter(user=request.user.username)

    if request.user.is_authenticated:
        profile = Profile.objects.get(user=request.user.username)
        context = {'profile': profile, 'voted_items':voted_items, 'last5_votes': last5_votes, 'top10': top10, 'wkfeeders':wkfeeders, 'selectedwk':selectedwk, 'form_auth': form_auth, 'form_yt':form_yt, 'form_wk': form_wk, 'in_main': in_main, 'selectedyt':selectedyt, 'ytfeeders':ytfeeders}
    else:
        context = {'voted_items':voted_items, 'last5_votes': last5_votes, 'top10': top10, 'wkfeeders':wkfeeders, 'selectedwk':selectedwk, 'form_auth': form_auth, 'form_yt':form_yt, 'form_wk': form_wk, 'in_main': in_main, 'selectedyt':selectedyt, 'ytfeeders':ytfeeders}
    return render(request,'miscosas/content.html',context)

def log_out(request):
    logout(request)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@csrf_exempt
def register(request):
    global in_main

    form = UserCreationForm()
    form_auth = AuthenticationForm()

    if request.method == "POST":
        try:
            action = request.POST['action']
        except:
            return redirect('notfound')

        if action == 'Registrarse':
            form = UserCreationForm(data=request.POST)
            if form.is_valid():
                user = form.save()

                if user is not None:
                    login(request, user)

                    try:
                        p = Profile.objects.get(user=user.username)
                        p.user = user.username
                    except Profile.DoesNotExist:
                        p = Profile(user=user.username)
                    p.save()

                    return redirect('main')

        elif action == 'IniciarSesion':
            form_auth = AuthenticationForm(data=request.POST)
            if form_auth.is_valid():
                username = form_auth.cleaned_data['username']
                password = form_auth.cleaned_data['password']

                user = authenticate(username=username, password=password)

                if user is not None:
                    login(request, user)
                    return redirect('registro')

    in_main = False
    context = {'form': form, 'form_auth':form_auth,'in_main': in_main}
    return render(request,'miscosas/register.html',context)

@csrf_exempt
def feeders(request):
    global in_main

    form_auth = AuthenticationForm()

    if request.method == "POST":
        try:
            action = request.POST['action']
        except:
            return redirect('notfound')

        if action == "IniciarSesion":
            form_auth = AuthenticationForm(data=request.POST)
            if form_auth.is_valid():
                username = form_auth.cleaned_data['username']
                password = form_auth.cleaned_data['password']

                user = authenticate(username=username, password=password)

                if user is not None:
                    login(request, user)
                    return redirect('alimentadores')

    ytfeeders = YtFeeders.objects.all()
    wkfeeders = WkFeeders.objects.all()
    in_main = False

    if request.user.is_authenticated:
        profile = Profile.objects.get(user=request.user.username)
        context = {'profile': profile, 'wkfeeders':wkfeeders ,'ytfeeders': ytfeeders, 'in_main':in_main, 'form_auth':form_auth}
    else:
        context = {'wkfeeders':wkfeeders ,'ytfeeders': ytfeeders, 'in_main':in_main, 'form_auth':form_auth}
    return render(request,'miscosas/feeders.html',context)

@csrf_exempt
def users(request):
    global in_main

    form_auth = AuthenticationForm()

    if request.method == "POST":
        try:
            action = request.POST['action']
        except:
            return redirec('notfound')

        if action == "IniciarSesion":
            form_auth = AuthenticationForm(data=request.POST)

            if form_auth.is_valid():
                username = form_auth.cleaned_data['username']
                password = form_auth.cleaned_data['password']

                user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('usuarios')

    in_main = False
    profiles = Profile.objects.all()

    if request.user.is_authenticated:
        profile = Profile.objects.get(user=request.user.username)
        context = {'profile': profile,'profiles':profiles, 'in_main': in_main, 'form_auth':form_auth}
    else:
        context = {'profiles':profiles, 'in_main': in_main, 'form_auth':form_auth}
    return render(request, 'miscosas/users.html', context)

@csrf_exempt
def user(request, username):
    global message
    global in_main

    form_auth = AuthenticationForm()
    form_photo = PhotoForm()
    form_style = StyleForm()
    form_letter = LetterForm()

    if request.method == "POST":
        try:
            action = request.POST['action']
        except:
            return redirect('notfound')

        if action == "IniciarSesion":
            form_auth = AuthenticationForm(data=request.POST)

            if form_auth.is_valid():
                username = form_auth.cleaned_data['username']
                password = form_auth.cleaned_data['password']

                user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('usuarios')

        elif action == "EnviarFoto":
            form_photo = PhotoForm(request.POST, request.FILES)

            if form_photo.is_valid():
                photo = request.FILES.get('photo')
                profile = Profile.objects.get(user=username)
                profile.photo = photo
                profile.save(update_fields=['photo'])

        elif action == "EnviarEstilo":
            form_style = StyleForm(data=request.POST)

            if form_style.is_valid():
                style = form_style.cleaned_data['Estilo']

                profile = Profile.objects.get(user=username)
                message = ''

                if style == 'ligero' or style == 'Ligero':
                    profile.style = 'light'
                    profile.save(update_fields=['style'])
                elif style == 'oscuro' or style == 'Oscuro':
                    profile.style = 'dark'
                    profile.save(update_fields=['style'])
                else:
                    message = '¡ Estilo de página no disponible !'

        elif action == "EnviarLetra":
            form_letter = LetterForm(data=request.POST)

            if form_letter.is_valid():
                letter = form_letter.cleaned_data['Tamaño_de_la_letra']

                profile = Profile.objects.get(user=username)
                message = ''

                if letter == 'pequeña' or letter == 'Pequeña':
                    profile.letter = 'small'
                    profile.save(update_fields=['letter'])
                elif letter == 'normal' or letter == 'Normal':
                    profile.letter = 'normal'
                    profile.save(update_fields=['letter'])
                elif letter == 'grande' or letter == 'Grande':
                    profile.letter = 'big'
                    profile.save(update_fields=['letter'])
                else:
                    message = '¡ Tamaño de letra no disponible !'

    try:
        profile = Profile.objects.get(user=username)
    except Profile.DoesNotExist:
        return redirect('notfound')

    voteditems_like = VotedItems.objects.all().filter(user=username, n_times_like=1)
    voteditems_dislike = VotedItems.objects.all().filter(user=username, n_times_dislike=1)
    comments = Comments.objects.all().filter(user=username)

    ytfeeders = YtFeeders.objects.all()
    ytitems = YtFeederItems.objects.all()
    wkfeeders = YtFeeders.objects.all()
    wkitems = YtFeederItems.objects.all()
    photo = profile.photo

    in_main = False
    context = {'photo':photo, 'form_style': form_style, 'form_letter': form_letter, 'message': message, 'username': username, 'form_photo': form_photo, 'profile': profile, 'ytfeeders': ytfeeders,
        'in_main': in_main, 'form_auth':form_auth, 'voteditems_dislike':voteditems_dislike, 'voteditems_like':voteditems_like, 'ytitems': ytitems,
        'wkitems': wkitems, 'comments': comments, 'wkfeeders': wkfeeders}
    return render(request, 'miscosas/user.html', context)

@csrf_exempt
def information(request):
    global in_main

    form_auth = AuthenticationForm()

    if request.method == "POST":
        try:
            action = request.POST['action']
        except:
            return redirect('notfound')
        if action == "IniciarSesion":
            form_auth = AuthenticationForm(data=request.POST)

            if form_auth.is_valid():
                username = form_auth.cleaned_data['username']
                password = form_auth.cleaned_data['password']

                user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('informacion')

    in_main = False

    if request.user.is_authenticated:
        profile = Profile.objects.get(user=request.user.username)
        context = {'profile': profile, 'in_main': in_main, 'form_auth': form_auth}
    else:
        context = {'in_main': in_main, 'form_auth': form_auth}

    return render(request, 'miscosas/information.html', context)

def main_xml(request):
    top_organized = TopPuntuationItems.objects.filter(puntuation__gt=0).order_by('-puntuation')
    top10 = top_organized.filter()[:10]
    votes_organized = VotedItems.objects.filter(user=request.user.username, date__isnull=False).order_by('-date')
    last5_votes = votes_organized.filter()[:5]
    ytfeeders = YtFeeders.objects.all()
    wkfeeders = WkFeeders.objects.all()
    selectedyt = ytfeeders.filter(selected=True)
    selectedwk = wkfeeders.filter(selected=True)

    context = {'top10': top10, 'last5': last5_votes, 'ytfeeders': ytfeeders, 'wkfeeders': wkfeeders, 'selectedyt': selectedyt, 'selectedwk': selectedwk}
    return render(request, 'miscosas/main.xml', context, content_type='text/xml')

def main_json(request):
    top_organized = TopPuntuationItems.objects.filter(puntuation__gt=0).order_by('-puntuation')
    top10 = top_organized.filter()[:10]
    votes_organized = VotedItems.objects.filter(user=request.user.username, date__isnull=False).order_by('-date')
    last5_votes = votes_organized.filter()[:5]
    ytfeeders = YtFeeders.objects.all()
    wkfeeders = WkFeeders.objects.all()
    selectedyt = ytfeeders.filter(selected=True)
    selectedwk = wkfeeders.filter(selected=True)

    context = {'top10': top10, 'last5': last5_votes, 'ytfeeders': ytfeeders, 'wkfeeders': wkfeeders, 'selectedyt': selectedyt, 'selectedwk': selectedwk}
    return render(request, 'miscosas/main.json', context, content_type='text/json')

def notfound(request):
    return render(request, 'miscosas/notfound.html', {})

#----------------------------------------------------------YOUTUBE-----------------------------------------------------------------------------------------------------

@csrf_exempt
def yt_feeder(request, feeder_id):
    global in_main

    form_auth = AuthenticationForm()

    try:
        ytfeeder = YtFeeders.objects.get(channel_id=feeder_id)
    except YtFeeders.DoesNotExist:
        return redirect('notfound')

    if request.method == "POST":
        try:
            action = request.POST['action']
        except:
            return redirec('notfound')

        if action == "IniciarSesion":
            form_auth = AuthenticationForm(data=request.POST)

            if form_auth.is_valid():
                username = form_auth.cleaned_data['username']
                password = form_auth.cleaned_data['password']

                user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        elif action == "SeleccionarYt":
            if not ytfeeder.selected:
                ytfeeder.selected = True
            else:
                ytfeeder.selected = False
            ytfeeder.save(update_fields=["selected"])

        elif action == "ActualizarYt":
            url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' + feeder_id
            update = True
            parseryt(url, feeder_id, update)

    ytfeeder_items = YtFeederItems.objects.all().filter(channel_id=feeder_id)
    ytfeeder = YtFeeders.objects.get(channel_id=feeder_id)
    in_main = False

    if request.user.is_authenticated:
        profile = Profile.objects.get(user=request.user.username)
        context = {'profile': profile, 'ytfeeder': ytfeeder, 'ytitems':ytfeeder_items, 'in_main':in_main, 'form_auth':form_auth}
    else:
        context = {'ytfeeder': ytfeeder, 'ytitems':ytfeeder_items, 'in_main':in_main, 'form_auth':form_auth}
    return render(request,'miscosas/ytfeeder.html', context)

@csrf_exempt
def yt_item(request, feeder_id, item_id):
    global in_main

    form_auth = AuthenticationForm()
    form_comment = CommentForm()
    url_item_page = 'http://localhost:8000/miscosas/youtube/' + feeder_id + '/' + item_id

    try:
        ytitem = YtFeederItems.objects.get(channel_id=feeder_id, video_id=item_id)
    except YtFeederItems.DoesNotExist:
        return redirect('notfound')


    if request.user.is_authenticated:
        try:
            v = VotedItems.objects.get(user=request.user.username, item_title=ytitem.video_title, item_link=ytitem.video_link)
            v.user = request.user.username
            v.item_title = ytitem.video_title
            v.item_link = ytitem.video_link
            v.item_page = url_item_page
        except VotedItems.DoesNotExist:
            v = VotedItems(user=request.user.username, item_title=ytitem.video_title, item_link=ytitem.video_link, item_page=url_item_page)
        v.save()

    if request.method == "POST":
        try:
            action = request.POST['action']
        except:
            return redirect('notfound')

        if action == "IniciarSesion":
            form_auth = AuthenticationForm(data=request.POST)

            if form_auth.is_valid():
                username = form_auth.cleaned_data['username']
                password = form_auth.cleaned_data['password']

                user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        elif action == "MeGustaYt":
            vote_user = VotedItems.objects.get(user=request.user.username, item_title=ytitem.video_title, item_link=ytitem.video_link)
            vote_user.date = timezone.now()

            ytfeeder = YtFeeders.objects.get(channel_id=feeder_id)
            ytfeeder_item = YtFeederItems.objects.get(video_id=item_id)

            if vote_user.n_times_like == 0:
                if vote_user.n_times_dislike == 0:
                    p = Profile.objects.get(user=request.user.username)
                    p.n_votes = p.n_votes + 1
                    p.save(update_fields=["n_votes"])
                elif vote_user.n_times_dislike == 1:
                    if ytfeeder_item.dislikes > 0:
                        ytfeeder_item.dislikes = ytfeeder_item.dislikes - 1

                vote_user.n_times_like = 1
                vote_user.n_times_dislike = 0

                ytfeeder.puntuation = ytfeeder.puntuation + 1
                ytfeeder_item.likes = ytfeeder_item.likes + 1

                if ytfeeder_item.likes > ytfeeder_item.dislikes:
                    ytfeeder_item.puntuation = ytfeeder_item.likes - ytfeeder_item.dislikes
                else:
                    ytfeeder_item.puntuation = 0

                ytfeeder.save(update_fields=["puntuation"])
                ytfeeder_item.save(update_fields=["likes", "dislikes", "puntuation"])
                vote_user.save(update_fields=["n_times_like", "n_times_dislike", "date"])

        elif action == "NoMeGustaYt":
            vote_user = VotedItems.objects.get(user=request.user.username, item_title=ytitem.video_title, item_link=ytitem.video_link)
            vote_user.date = timezone.now()

            ytfeeder = YtFeeders.objects.get(channel_id=feeder_id)
            ytfeeder_item = YtFeederItems.objects.get(video_id=item_id)

            if vote_user.n_times_dislike == 0:
                if vote_user.n_times_like == 0:
                    p = Profile.objects.get(user=request.user.username)
                    p.n_votes = p.n_votes + 1
                    p.save(update_fields=["n_votes"])
                elif vote_user.n_times_like == 1:
                    if ytfeeder_item.likes > 0:
                        ytfeeder_item.likes = ytfeeder_item.likes - 1

                vote_user.n_times_like = 0
                vote_user.n_times_dislike = 1

                ytfeeder_item.dislikes = ytfeeder_item.dislikes + 1

                if ytfeeder.puntuation > 0:
                    ytfeeder.puntuation =  ytfeeder.puntuation - 1

                if ytfeeder_item.likes > ytfeeder_item.dislikes:
                    ytfeeder_item.puntuation = ytfeeder_item.likes - ytfeeder_item.dislikes
                else:
                    ytfeeder_item.puntuation = 0

                ytfeeder.save(update_fields=["puntuation"])
                ytfeeder_item.save(update_fields=["dislikes", "likes", "puntuation"])
                vote_user.save(update_fields=["n_times_like", "n_times_dislike", "date"])

        elif action == "EnviarComentario":
            form_comment = CommentForm(request.POST, request.FILES)

            if form_comment.is_valid():
                body = form_comment.cleaned_data['body']
                photo = request.FILES.get('photo')
                c = Comments(item_title=ytitem.video_title, item_link=ytitem.video_link, user=request.user.username, body=body, date=timezone.now(), photo=photo)
                c.save()

                p = Profile.objects.get(user=request.user.username)
                p.n_comments = p.n_comments + 1
                p.save(update_fields=['n_comments'])

    ytfeeder = YtFeeders.objects.get(channel_id=feeder_id)
    ytfeeder_item = YtFeederItems.objects.get(video_id=item_id)
    comments = Comments.objects.all()
    in_main = False

    if request.user.is_authenticated:
        profile = Profile.objects.get(user=request.user.username)
        ytvote = VotedItems.objects.get(user=request.user.username, item_title=ytitem.video_title, item_link=ytitem.video_link)
        context = {'profile': profile, 'ytvote': ytvote, 'comments': comments, 'ytfeeder': ytfeeder, 'ytitem': ytfeeder_item, 'in_main': in_main, 'form_auth':form_auth, 'form_comment':form_comment}
    else:
        context = {'comments': comments, 'ytfeeder': ytfeeder, 'ytitem': ytfeeder_item, 'in_main': in_main, 'form_auth':form_auth, 'form_comment':form_comment}

    return render(request, 'miscosas/ytitem.html', context)

#--------------------------------------------------------------WIKIPEDIA-----------------------------------------------------------------------

@csrf_exempt
def wk_feeder(request, art_name):
    global in_main

    form_auth = AuthenticationForm()

    try:
        wkfeeder = WkFeeders.objects.get(article_name=art_name)
    except WkFeeders.DoesNotExist:
        return redirect('notfound')

    if request.method == "POST":
        try:
            action = request.POST['action']
        except:
            return redirect('notfound')

        if action == "IniciarSesion":
            form_auth = AuthenticationForm(data=request.POST)

            if form_auth.is_valid():
                username = form_auth.cleaned_data['username']
                password = form_auth.cleaned_data['password']

                user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        elif action == "SeleccionarWk":
            if not wkfeeder.selected:
                wkfeeder.selected = True
            else:
                wkfeeder.selected = False
            wkfeeder.save(update_fields=["selected"])

        elif action == "ActualizarWk":
            url = 'https://en.wikipedia.org/w/index.php?title=' + art_name + '&action=history&feed=rss'
            update = True
            parserwkfeed(url, art_name, update)
            parserwkitems(url, art_name)

    wkfeeder_items = WkFeederItems.objects.all().filter(article_name=art_name)
    wkfeeder = WkFeeders.objects.get(article_name=art_name)
    in_main = False

    if request.user.is_authenticated:
        profile = Profile.objects.get(user=request.user.username)
        context = {'profile': profile, 'wkfeeder': wkfeeder, 'wkitems':wkfeeder_items, 'in_main':in_main, 'form_auth':form_auth}
    else:
        context = {'wkfeeder': wkfeeder, 'wkitems':wkfeeder_items, 'in_main':in_main, 'form_auth':form_auth}
    return render(request,'miscosas/wkfeeder.html', context)

@csrf_exempt
def wk_item(request, art_name, item_id):
    global in_main

    form_auth = AuthenticationForm()
    form_comment = CommentForm()
    url_item_page = 'http://localhost:8000/miscosas/wikipedia/' + art_name + '/' + item_id

    try:
        wkitem = WkFeederItems.objects.get(id=item_id)
    except WkFeederItems.DoesNotExist:
        return redirect('notfound')

    if request.user.is_authenticated:
        try:
            v = VotedItems.objects.get(user=request.user.username, item_title=wkitem.change_title, item_link=wkitem.change_link)
            v.user = request.user.username
            v.item_title = wkitem.change_title
            v.item_link = wkitem.change_link
            v.item_page = url_item_page
        except VotedItems.DoesNotExist:
            v = VotedItems(user=request.user.username, item_title=wkitem.change_title, item_link=wkitem.change_link, item_page=url_item_page)
        v.save()

    if request.method == "POST":
        try:
            action = request.POST['action']
        except:
            return redirect('notfound')

        if action == "IniciarSesion":
            form_auth = AuthenticationForm(data=request.POST)

            if form_auth.is_valid():
                username = form_auth.cleaned_data['username']
                password = form_auth.cleaned_data['password']

                user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        elif action == "MeGustaWk":
            vote_user = VotedItems.objects.get(user=request.user.username, item_title=wkitem.change_title, item_link=wkitem.change_link)
            vote_user.date = timezone.now()

            wkfeeder = WkFeeders.objects.get(article_name=art_name)
            wkfeeder_item = WkFeederItems.objects.get(id=item_id)

            if vote_user.n_times_like == 0:
                if vote_user.n_times_dislike == 0:
                    p = Profile.objects.get(user=request.user.username)
                    p.n_votes = p.n_votes + 1
                    p.save(update_fields=["n_votes"])

                elif vote_user.n_times_dislike == 1:
                    if wkfeeder_item.dislikes > 0:
                        wkfeeder_item.dislikes = wkfeeder_item.dislikes - 1

                vote_user.n_times_like = 1
                vote_user.n_times_dislike = 0

                wkfeeder.puntuation = wkfeeder.puntuation + 1
                wkfeeder_item.likes = wkfeeder_item.likes + 1

                if wkfeeder_item.likes > wkfeeder_item.dislikes:
                    wkfeeder_item.puntuation = wkfeeder_item.likes - wkfeeder_item.dislikes
                else:
                    wkfeeder_item.puntuation = 0

                wkfeeder.save(update_fields=["puntuation"])
                wkfeeder_item.save(update_fields=["likes", "dislikes", "puntuation"])
                vote_user.save(update_fields=["n_times_like", "n_times_dislike", "date"])

        elif action == "NoMeGustaWk":
            vote_user = VotedItems.objects.get(user=request.user.username, item_title=wkitem.change_title, item_link=wkitem.change_link)
            vote_user.date = timezone.now()

            wkfeeder = WkFeeders.objects.get(article_name=art_name)
            wkfeeder_item = WkFeederItems.objects.get(id=item_id)

            if vote_user.n_times_dislike == 0:
                if vote_user.n_times_like == 0:
                    p = Profile.objects.get(user=request.user.username)
                    p.n_votes = p.n_votes + 1
                    p.save(update_fields=["n_votes"])

                elif vote_user.n_times_like == 1:
                    if wkfeeder_item.likes > 0:
                        wkfeeder_item.likes = wkfeeder_item.likes - 1

                vote_user.n_times_like = 0
                vote_user.n_times_dislike = 1

                wkfeeder_item.dislikes = wkfeeder_item.dislikes + 1

                if wkfeeder.puntuation > 0:
                    wkfeeder.puntuation =  wkfeeder.puntuation - 1

                if wkfeeder_item.likes > wkfeeder_item.dislikes:
                    wkfeeder_item.puntuation = wkfeeder_item.likes - wkfeeder_item.dislikes
                else:
                    wkfeeder_item.puntuation = 0

                wkfeeder.save(update_fields=["puntuation"])
                wkfeeder_item.save(update_fields=["dislikes", "likes", "puntuation"])
                vote_user.save(update_fields=["n_times_like", "n_times_dislike", "date"])

        elif action == "EnviarComentario":
            form_comment = CommentForm(request.POST, request.FILES)

            if form_comment.is_valid():
                body = form_comment.cleaned_data['body']
                photo = request.FILES.get('photo')
                c = Comments(item_title=wkitem.change_title, item_link=wkitem.change_link, user=request.user.username, body=body, date=timezone.now(), photo=photo)
                c.save()

                p = Profile.objects.get(user=request.user.username)
                p.n_comments = p.n_comments + 1
                p.save(update_fields=['n_comments'])

    wkfeeder = WkFeeders.objects.get(article_name=art_name)
    wkfeeder_item = WkFeederItems.objects.get(id=item_id)
    comments = Comments.objects.all()
    in_main = False

    if request.user.is_authenticated:
        profile = Profile.objects.get(user=request.user.username)
        wkvote = VotedItems.objects.get(user=request.user.username, item_title=wkitem.change_title, item_link=wkitem.change_link)
        context = {'profile': profile, 'item_id': item_id, 'wkvote': wkvote, 'comments': comments, 'wkfeeder': wkfeeder, 'wkitem': wkfeeder_item, 'in_main': in_main, 'form_auth':form_auth, 'form_comment':form_comment}
    else:
        context = {'item_id': item_id,'comments': comments, 'wkfeeder': wkfeeder, 'wkitem': wkfeeder_item, 'in_main': in_main, 'form_auth':form_auth, 'form_comment':form_comment}

    return render(request, 'miscosas/wkitem.html', context)

# Generated by Django 3.0.3 on 2020-05-28 07:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('miscosas', '0003_voteditems_vote_in_main'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='voteditems',
            name='vote_in_main',
        ),
        migrations.AddField(
            model_name='voteditems',
            name='item_page',
            field=models.TextField(default=''),
        ),
    ]

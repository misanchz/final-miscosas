from django.contrib.auth.models import User
from django.db import models

#ALIMENTADORES

class YtFeeders(models.Model):
    channel_name = models.TextField(default='')
    channel_id = models.TextField(default='')
    channel_link = models.TextField(default='')
    puntuation = models.IntegerField(default=0)
    items = models.IntegerField(default=0)
    selected = models.BooleanField(default=False)

    def __str__(self):
       return self.channel_name

class WkFeeders(models.Model):
    article_name = models.TextField()
    article_link = models.TextField()
    puntuation = models.IntegerField(default=0)
    items = models.IntegerField(default=0)
    selected = models.BooleanField(default=False)

    def __str__(self):
        return self.article_name

#ITEMS DE ALIMENTADORES

class YtFeederItems(models.Model):
    channel_id = models.TextField(default='')
    video_title = models.TextField(default='')
    video_id = models.TextField(default='')
    video_link = models.TextField(default='')
    description = models.TextField(default='')
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    puntuation = models.IntegerField(default=0)

    def __str__(self):
        return self.video_title

class WkFeederItems(models.Model):
    article_name = models.TextField()
    change_title = models.TextField()
    change_link = models.TextField()
    change_author = models.TextField()
    change_date = models.TextField()
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    puntuation = models.IntegerField(default=0)

#COMENTARIOS

class Comments(models.Model):
    item_title = models.TextField(default='')
    item_link = models.TextField(default='')
    user = models.TextField(default='')
    body = models.CharField(blank=True, max_length=256)
    date = models.DateTimeField(default='')
    photo = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.user

#VOTOS

class VotedItems(models.Model):
    user = models.TextField(default='')
    item_title = models.TextField(default='')
    item_link = models.TextField(default='')
    n_times_like = models.IntegerField(default=0)
    n_times_dislike = models.IntegerField(default=0)
    date = models.DateTimeField(null=True, blank=True)
    item_page = models.TextField(default='', null=True)

    def __str__(self):
        return self.user + '=>' + self.item_title

#PERFIL

class Profile(models.Model):
    user = models.TextField(default='')
    photo = models.ImageField(blank=True, null=True)
    n_votes = models.IntegerField(default=0)
    n_comments = models.IntegerField(default=0)
    style = models.TextField(default='light')
    letter = models.TextField(default='normal')

    def __str__(self):
        return self.user

#TOP TEN ITEMS

class TopPuntuationItems(models.Model):
    item_name = models.TextField(default='')
    item_link = models.TextField(default='')
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    puntuation = models.IntegerField(default=0)
    item_page = models.TextField(default='')

    def __str__(self):
        return self.item_name

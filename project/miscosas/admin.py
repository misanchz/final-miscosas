from django.contrib import admin
from .models import YtFeederItems, YtFeeders, Comments, VotedItems, WkFeeders, WkFeederItems, Profile, TopPuntuationItems


# Register your models here.
admin.site.register(YtFeederItems)
admin.site.register(YtFeeders)
admin.site.register(Comments)
admin.site.register(VotedItems)
admin.site.register(Profile)
admin.site.register(WkFeeders)
admin.site.register(WkFeederItems)
admin.site.register(TopPuntuationItems)

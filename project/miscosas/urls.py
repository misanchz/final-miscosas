from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.main, name="main"),
    path('notfound', views.notfound, name="notfound"),
    path('?format=xml', views.main_xml, name='xml'),
    path("?format=json", views.main_json, name="json"),
    path('logout', views.log_out, name="logout"),
    path('registro', views.register, name="registro"),
    path('alimentadores', views.feeders, name="alimentadores"),
    path('informacion', views.information, name="informacion"),
    path('usuarios', views.users, name="usuarios"),
    path('usuarios/<str:username>', views.user),
    path('youtube/<str:feeder_id>/<str:item_id>', views.yt_item),
    path('youtube/<str:feeder_id>', views.yt_feeder),
    path('wikipedia/<str:art_name>/<str:item_id>', views.wk_item),
    path('wikipedia/<str:art_name>', views.wk_feeder),
]
